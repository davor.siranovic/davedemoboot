package demo.example.beans;

public class DemoBean {
	private String r1 = "";
	private String r2 = "";
	
	
	public DemoBean(String r1, String r2) {
		super();
		this.r1 = r1;
		this.r2 = r2;
	}
	public String getR1() {
		return r1;
	}
	public void setR1(String r1) {
		this.r1 = r1;
	}
	public String getR2() {
		return r2;
	}
	public void setR2(String r2) {
		this.r2 = r2;
	}
	
}
