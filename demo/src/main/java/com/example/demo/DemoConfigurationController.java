package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import demo.example.beans.DemoBean;

@RestController
public class DemoConfigurationController {
	@GetMapping("/demoOne")
	public DemoBean getDemoBeanOut() {
		DemoBean demoBean = new DemoBean("first", "second");
		return demoBean;
	}
}
